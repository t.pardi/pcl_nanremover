#include <cstdlib>
// pcl
#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/ascii_io.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>

#include <iostream>


int main(int argc, char** argv) 
{ 
    
	std::string path_in = argv[1];
	std::string path_out = argv[2];

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in (new pcl::PointCloud<pcl::PointXYZ>);

	std::cout << path_in << std::endl;

	if (pcl::io::loadPCDFile<pcl::PointXYZ> (path_in.c_str(), *cloud_in) == -1) //* load the file
	{
		PCL_ERROR ("Couldn't read file %s\n", path_in);
		return false;
	}

 	std::vector<int> indices;

	pcl::removeNaNFromPointCloud(*cloud_in, *cloud_in, indices);

	std::cout << "Loaded " << cloud_in->width * cloud_in->height << " data points from " << path_in << std::endl;

	std::cout << path_out << std::endl;

	pcl::io::savePCDFileASCII(path_out, *cloud_in);

	return true;
}
